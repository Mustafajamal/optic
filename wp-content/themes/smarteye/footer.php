<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage TemplateMela
 * @since TemplateMela 1.0
 */
?>
<?php tm_content_after(); ?>
</div>
<!-- .main-content-inner -->
</div>
<!-- .main_inner -->
</div>
<!-- #main -->
<?php tm_footer_before(); ?>
<footer id="colophon" class="site-footer">
	
	<?php if ( is_active_sidebar( 'footer-top-area' ) ) : ?>
			<div class="footer-top">
				<div class="footer-top-inner">
					<?php dynamic_sidebar('footer-top-area'); ?>
				</div>
			</div>
	<?php endif; ?>
	  <div class="theme-container">
		  <div class="footer_inner">
			
			<?php tm_footer_inside(); ?>
			<?php get_sidebar('footer'); ?>
			
			<!-- .footer-bottom -->
		  </div>
		  <div class="footer-bottom">
		  <div class="footer-center">
				
				<?php if ( has_nav_menu('footer-menu') ) { ?>    
				<div class="footer-menu-links">
				<?php
							$tm_footer_menu=array(
							'menu' => 'TM Footer Navigation',
							'depth'=> 1,
							'echo' => false,
							'menu_class'      => 'footer-menu', 
							'container'       => '', 
							'container_class' => '', 
							'theme_location' => 'footer-menu'
							);
							echo wp_nav_menu($tm_footer_menu);				    
							?>
				</div><!-- #footer-menu-links -->	
				<?php } ?> 
			   		  
					  <div class="site-info">  <?php esc_html__( 'Copyright', 'smarteye' ); ?> &copy; <?php echo esc_attr(date('Y')); ?> <a href="<?php echo esc_url(get_option('tm_footer_link')); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo esc_attr(get_option('tm_footer_slog'));?>
						</a>
						<?php do_action( 'tm_credits' ); ?>
					  </div>
				
				<?php if (is_active_sidebar('footer-bottom-widget-area')) : ?>
				<div class="footer-bottom-inner">	
					<?php dynamic_sidebar('footer-bottom-widget-area'); ?>
				</div>
				<?php endif; ?>
		
			 
			  
			</div>
			</div>
	</div>


<?php 

if ( is_product() ){

?>
<script>
jQuery(document).ready(function() {  
jQuery(".select_notice1-div").hide();
jQuery(".select_notice2-div").hide();
jQuery(".select_notice3-div").hide();

  //First Logic
  jQuery('#tmcp_select_1, #tmcp_select_4').change(function(){

	  	 var selected_val1 = parseInt(jQuery('#tmcp_select_1').val());
	     var selected_val4 = parseInt(jQuery('#tmcp_select_4').val());

		 if(selected_val1 < 0){
	         console.log("First Negative");
	         var right_lense = 'negative';
	      }
	      else if(selected_val1 > 0){
	         console.log("First Positive");
	         var right_lense = 'positive';
	      }
	      if(selected_val4 < 0){
	         console.log("Second Negative");
	         var left_lense = 'negative';
	      }
	      else if(selected_val4 > 0){
	          console.log("Second Positive");
	          var left_lense = 'positive';
	      }

	      //Showing message
	      if(right_lense == 'positive' && left_lense == 'negative') {
	      	console.log("First message show");
	      	jQuery(".select_notice1-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);
	      }
	      else if(left_lense == 'positive' && right_lense == 'negative') {
	      	console.log("First message show");
	      	jQuery(".select_notice1-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);
	      }
	      else if(left_lense == 'positive' && right_lense == 'positive') {
	      	console.log("First message hide");
	      	jQuery(".select_notice1-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }
	      else if(left_lense == 'negative' && right_lense == 'negative') {
	      	console.log("First message hide");
	      	jQuery(".select_notice1-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }      
  });


  //Second Logic
  jQuery('#tmcp_select_2, #tmcp_select_5').change(function(){

	  	 var selected_val2 = parseInt(jQuery('#tmcp_select_2').val());
	     var selected_val5 = parseInt(jQuery('#tmcp_select_5').val());

		 if(selected_val2 < 0){
	         console.log("First Negative");
	         var right_lense2 = 'negative';
	      }
	      else if(selected_val2 > 0){
	         console.log("First Positive");
	         var right_lense2 = 'positive';
	      }
	      if(selected_val5 < 0){
	         console.log("Second Negative");
	         var left_lense2 = 'negative';
	      }
	      else if(selected_val5 > 0){
	          console.log("Second Positive");
	          var left_lense2 = 'positive';
	      }

	      //Showing message
	      if(right_lense2 == 'positive' && left_lense2 == 'negative') {
	      	console.log("First message show");
	      	jQuery(".select_notice2-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);

	      }
	      else if(left_lense2 == 'positive' && right_lense2 == 'negative') {
	      	console.log("First message show");
	      	jQuery(".select_notice2-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);

	      }
	      else if(left_lense2 == 'positive' && right_lense2 == 'positive') {
	      	console.log("First message hide");
	      	jQuery(".select_notice2-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }
	      else if(left_lense2 == 'negative' && right_lense2 == 'negative') {
	      	console.log("First message hide");
	      	jQuery(".select_notice2-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }      
  });

  

  //Axis Selection 
  jQuery('#tmcp_select_3, #tmcp_select_6,#tmcp_select_2,#tmcp_select_5').change(function(){

	var selected_val2 = parseInt(jQuery('#tmcp_select_3').val());
	var selected_val5 = parseInt(jQuery('#tmcp_select_6').val());
	var selected_val22 = parseInt(jQuery('#tmcp_select_2').val());
	var selected_val55 = parseInt(jQuery('#tmcp_select_5').val());

		console.log(selected_val2);
		console.log(selected_val5);
	if(selected_val2 == 0 || selected_val5 == 0 ){
	 //console.log("Please choose numeric values or 0 for both CYL (Cylinder) and Axis.");
	 	jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);
	}
	else if(selected_val2 > 0 && selected_val5 > 0 ){    
		 jQuery(".select_notice3-div").hide();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
		 console.log("Greater then zero");    
	} 
	else if(selected_val2 < 0 && selected_val5 < 0 ){ 
		 jQuery(".select_notice3-div").hide();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
		 console.log("Less then zero");
	}
	else if(selected_val2 == 0 && selected_val5 > 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	else if(selected_val2 == 0 && selected_val5 < 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	else if(selected_val5 == 0 && selected_val2 > 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	else if(selected_val5 == 0 && selected_val2 < 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	
	
	// Hassan COde
	//CYL  Selection
	if(selected_val22 == 0 || selected_val55 == 0 ){
	 //console.log("Please choose numeric values or 0 for both CYL (Cylinder) and Axis.");
	 	jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);
	}
	else if(selected_val22 > 0 && selected_val55 > 0 ){    
		 jQuery(".select_notice3-div").hide();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
		 console.log("Greater then zero");    
	} 
	else if(selected_val22 < 0 && selected_val55 < 0 ){ 
		 jQuery(".select_notice3-div").hide();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
		 console.log("Less then zero");
	}
	else if(selected_val22 == 0 && selected_val55 > 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	else if(selected_val22 == 0 && selected_val55 < 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	else if(selected_val55 == 0 && selected_val22 > 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	else if(selected_val55 == 0 && selected_val22 < 0 ){ 
		 jQuery(".select_notice3-div").show();
		 jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);		 
	}
	
  });

  //Multifocal Check
   jQuery(".multifocalcheck-div").hide();
   jQuery('#tmcp_select_1, #tmcp_select_4').change(function(){
  
      var selected_val1_m = parseInt(jQuery('#tmcp_select_1').val());
      var selected_val4_m = parseInt(jQuery('#tmcp_select_4').val());
      //console.log(selected_val1_m);
      //console.log(selected_val4_m);
      
      //Range Check Function
      Number.prototype.between  = function (a, b) {
        var min = Math.min.apply(Math, [a,b]),
        max = Math.max.apply(Math, [a,b]);
        return this >= min && this <= max;
      };
      
     //SPHOD Range check 
      var inside_range = selected_val1_m.between(6, -10);
      //console.log("inside_range..."+inside_range);
      
      if(inside_range == true  ){ 
         //console.log("inside inside_range True");
         setTimeout(function() {
          jQuery(".multifocalcheck-div").show();
         }, 2000); 
      }else if(inside_range == false){ 
         //console.log("inside inside_range False");
         setTimeout(function() {  
          document.getElementById("multifocalcheck").style.display = "none";
          jQuery("#multifocalcheck").hide();  
         }, 3000);
               
      }

     //Sphos Range check 
      var inside_range_sphos = selected_val4_m.between(6, -10);
      
      if(inside_range_sphos == true  ){ 
         setTimeout(function() {
          jQuery(".multifocalcheck-div").show();
         }, 2000);
      }else if(inside_range_sphos == false){ 
         setTimeout(function() {
          jQuery(".multifocalcheck-div").hide();
         }, 2000);        
      }



   });

   //Zero Login Final Upper
	jQuery('#tmcp_select_2, #tmcp_select_3').change(function(){

	  	 var selected_val2 = parseInt(jQuery('#tmcp_select_2').val());
	     var selected_val3 = parseInt(jQuery('#tmcp_select_3').val());

	     console.log(selected_val2);
	     console.log(selected_val3);

		 if(selected_val2 < 0 || selected_val2 > 0){
	         console.log("First Negative");
	         var right_lense2 = 'numeric';
	      }		      
	     if(selected_val2 == 0){
	         console.log("First Zero");
	         var right_lense2 = 'zero' ;
	     }
	      
	     if(selected_val3 > 0 || selected_val3 < 0){
	          console.log("Second Positive");
	          var left_lense2 = 'numeric';
	      }		     
	     if(selected_val3 == 0){
	          console.log("Second Zero");
	          var left_lense2 = 'zero';
	      }

	      //Showing Error message
	      if(right_lense2 == 'numeric' && left_lense2 == 'zero') {
	      	console.log("Right num left zero");
	      	jQuery(".select_notice3-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);

	      }
	     
	      else if(left_lense2 == 'zero' && right_lense2 == 'numeric') {
	      	console.log(" Left Zero right Numeric ");
	      	jQuery(".select_notice3-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);

	      }
	     
	      //Hiding Message
	      else if(left_lense2 == 'numeric' && right_lense2 == 'numeric') {
	      	console.log("Both Numeric");
	      	jQuery(".select_notice3-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }
	      else if(left_lense2 == 'zero' && right_lense2 == 'zero') {
	      	console.log("Both Zero");
	      	jQuery(".select_notice3-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }      
	});

   //Zero Login Final Lower
	jQuery('#tmcp_select_5, #tmcp_select_6').change(function(){

	  	 var selected_val5 = parseInt(jQuery('#tmcp_select_5').val());
	     var selected_val6 = parseInt(jQuery('#tmcp_select_6').val());

	     console.log(selected_val5);
	     console.log(selected_val6);

		 if(selected_val5 < 0 || selected_val5 > 0){
	         console.log("First Negative");
	         var right_lense2 = 'numeric';
	      }		      
	     if(selected_val5 == 0){
	         console.log("First Zero");
	         var right_lense2 = 'zero' ;
	     }
	      
	     if(selected_val6 > 0 || selected_val6 < 0){
	          console.log("Second Positive");
	          var left_lense2 = 'numeric';
	      }		     
	     if(selected_val6 == 0){
	          console.log("Second Zero");
	          var left_lense2 = 'zero';
	      }

	      //Showing Error message
	      if(right_lense2 == 'numeric' && left_lense2 == 'zero') {
	      	console.log("Right num left zero");
	      	jQuery(".select_notice3-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);

	      }
	     
	      else if(left_lense2 == 'zero' && right_lense2 == 'numeric') {
	      	console.log(" Left Zero right Numeric ");
	      	jQuery(".select_notice3-div").show();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', true);

	      }
	     
	      //Hiding Message
	      else if(left_lense2 == 'numeric' && right_lense2 == 'numeric') {
	      	console.log("Both Numeric");
	      	jQuery(".select_notice3-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }
	      else if(left_lense2 == 'zero' && right_lense2 == 'zero') {
	      	console.log("Both Zero");
	      	jQuery(".select_notice3-div").hide();
	      	jQuery('.single_add_to_cart_button, button.product_type_simple').prop('disabled', false);
	      }      
	});








});

</script>
<?php

}
?>


  <!--. Footer inner -->
</footer>
<!-- #colophon -->
<?php tm_footer_after(); ?>
</div>
<!-- #page -->
<?php smarteye_gotop(); ?>
<?php smarteye_get_widget('before-end-body-widget'); ?>
<?php wp_footer(); ?>
</body>
</html>
